const loanElement = document.getElementById("loan");
const priceElement = document.getElementById("price");
const specsElement = document.getElementById("specs");
const salaryElement = document.getElementById("salary");
const balanceElement = document.getElementById("balance");
const computersElement = document.getElementById("computers");
const descriptionElement = document.getElementById("description");
const computerNameElement = document.getElementById("computerName");
const computerImageElement = document.getElementById("computerImage");
const buyButtonElement = document.getElementById("buy");
const bankButtonElement = document.getElementById("bank");
const workButtonElement = document.getElementById("work");
const payLoanButtonElement = document.getElementById("payLoan");
const getLoanButtonElement = document.getElementById("getLoan");

let computers = [];
let cart = [];
let totalDue = 0.0;
let balance = 0;
let salary = 0;
let loan = 0;

/* Fetch API data */
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToMenu(computers));

/* Adds the info to the big card on the screen */
const addComputersToMenu = (computers) => {
  computers.forEach((x) => addComputerToMenu(x));
  computerNameElement.innerText = computers[0].title;
  computerImageElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + computers[0].image;
  priceElement.innerText = computers[0].price + " Kr";
  updateSpecs(computers[0].specs);
  descriptionElement.innerText = computers[0].description;
};

/* Adds a computer so it can be shown */
const addComputerToMenu = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersElement.appendChild(computerElement);
};

/* Handler for the selector */
const handleComputerMenuChange = (e) => {
  const selectedComputer = computers[e.target.selectedIndex];
  priceElement.innerText = selectedComputer.price;
  computerNameElement.innerText = selectedComputer.title;
  computerImageElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
  if (selectedComputer == computers[4]) {
    /* A little easter egg because "The Visor" do not have an image */
    computerImageElement.src =
      "https://dainese-cdn.thron.com/delivery/public/image/dainese/f618f8c0-0088-4e08-b5bd-64d38a724929/ramfdh/std/615x615/visor-gt3-2-tinted.jpg";
  }
  priceElement.innerText = selectedComputer.price + " Kr";
  updateSpecs(selectedComputer.specs);
  descriptionElement.innerText = selectedComputer.description;
};

/* Get Loan Button */
const handleGetLoanButton = () => {
  if (loan > 0) {
    /* Cannot take two loans */
    alert(
      `You already have a loan of ${loan} Kr and have to pay it down to get another.`
    );
  } else {
    const totalGetLoan = prompt(
      "Please enter the amount of money you want to loan: "
    );
    if (totalGetLoan == null || isNaN(totalGetLoan)) {
      loan = 0;
      alert(`You have to put an integer as Input`);
    } else {
      if (totalGetLoan > balance * 2) {
        alert(`You cannot take a loan of double your balance!`);
      } else {
        loan += parseInt(totalGetLoan);
        balance += loan;
        balanceElement.innerText = `Balance: ${balance} Kr.`;
        loanElement.innerText = `Outstanding loan: ${loan} Kr.`;
        loanElement.style.visibility = "visible";
        payLoanButtonElement.style.visibility = "visible";
        alert(
          `You took a loan of ${totalGetLoan} Kr and your total loan is ${loan} Kr.`
        );
      }
    }
  }
};

/* Pay Loan Button */
const handlePayLoanButton = () => {
  if (salary > loan) {
    /* Update values */
    balance += salary - loan;
    loan = 0;
    salary = 0;
  } else {
    loan = loan - salary;
  }

  /* Update salary */
  salary = 0;
  salaryElement.innerText = `Salary: ${salary} Kr.`;

  balanceElement.innerText = `Balance: ${balance} Kr.`;
  loanElement.innerText = `Outstanding loan: ${loan} Kr.`;

  if (loan == 0) {
    loanElement.style.visibility = "hidden";
    payLoanButtonElement.style.visibility = "hidden";
  }
};

/* Bank Button */
const handleBankButton = () => {
  if (loan > 0) {
    /* Check if customer has a loan */
    loan = loan - salary * 0.1; /* Pay loan with 10% of the salary */
    balance += salary - salary * 0.1; /* Remove 10% of the salary */
  } else {
    balance += salary; /* No loan -> everything goes to the balance */
  }
  balanceElement.innerText = `Balance: ${balance} Kr.`;

  /* Update salary */
  salary = 0;
  salaryElement.innerText = `Salary: ${salary} Kr.`;

  /* Update loan */
  loanElement.innerText = `Outstanding loan: ${loan} Kr.`;
};

/* Work Button */
const handleWorkButton = () => {
  salary += 100;
  salaryElement.innerText = `Salary: ${salary} Kr.`;
};

/* Buy Button */
const handleBuyButton = () => {
  /*balanceElement = balanceElement += 100;*/
  let computer = document.getElementById("computers");
  computer = computers[computer.value - 1];
  if (balance >= computer.price) {
    balance = balance - computer.price;
    alert(`You bought a ${computer.title}!`);
  } else {
    /* You are poor and cannot afford this computer */
    alert(
      `You are poor and cannot afford ${computer.title}!\nWork or take a loan!`
    );
  }
  balanceElement.innerText = `Balance: ${balance} Kr.`;
};

/* Get computer specs in a list */
function updateSpecs(arr) {
  specsElement.innerText = "";
  for (let i of arr) {
    let li = document.createElement("li");
    li.appendChild(document.createTextNode(i));
    specsElement.appendChild(li);
  }
}

/* Event listeners */
computersElement.addEventListener("change", handleComputerMenuChange);
getLoanButtonElement.addEventListener("click", handleGetLoanButton);
bankButtonElement.addEventListener("click", handleBankButton);
workButtonElement.addEventListener("click", handleWorkButton);
buyButtonElement.addEventListener("click", handleBuyButton);
payLoanButtonElement.addEventListener("click", handlePayLoanButton);
