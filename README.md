# Assignment 1 Frontend

This project contains **app.js**, **index.html** and **styles.css**. <br />
The project task is to make a website.

### Result
<img src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/apple/325/thumbs-up_1f44d.png"  width="20" height="20"> The functionality is fully working and has no problems.

<img src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/apple/325/thumbs-down_1f44e.png"  width="20" height="20"> Had some issues getting the cards in the same size.
